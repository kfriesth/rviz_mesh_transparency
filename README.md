![eff](/eff.png)

Dependencies
---
- [Robot Operating System](http://wiki.ros.org/ROS/Installation)
- [fanuc](http://wiki.ros.org/fanuc)

This package has been tested with Ubuntu 16.04 and ROS Kinetic.

Testing
---
`catkin build` (or `catkin_make`)

`roslaunch fanuc_m10ia_test_support test_m10ia_test_eef.launch`

Issue
---
https://github.com/ros-visualization/rviz/issues/995

